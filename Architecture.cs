﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace CourseWork
{
    public delegate void AddItem(object Item);
    public class Company : IComparable
    {
        [Required(ErrorMessage = "Имя компании не установлено")]
        public string CompanyName { get; set; }
        public DateTime CreationDate { get; set;}

        public Company() : this("Компания не задана") { }
        public Company(string companyName) : this(companyName, DateTime.Now) { }
        public Company(string companyName, DateTime creationDate)
        {
            CompanyName = companyName;
            CreationDate = creationDate;
        }

        public virtual int CompareTo(object o)
        {
            Company company = o as Company;
            if (company != null)
                return CompanyName.CompareTo(company.CompanyName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }
    }
    public class CategoryCPU : Company
    {
        [Required(ErrorMessage = "Название категории не установлено")]
        public string Category { get; set; }

        //Конструкторы
        public CategoryCPU(string category) : base()
        {
            Category = category;
        }
        public CategoryCPU(string company, string category) :
            base(company)
        {
            Category = category;
        }
        public CategoryCPU(string company, DateTime creationDate, string category) :
            base(company, creationDate)
        {
            Category = category;
        }
        public override int CompareTo(object o)
        {
            CategoryCPU category = o as CategoryCPU;
            if (category != null)
                return Category.CompareTo(category.Category);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }
    }
    public class Processor : CategoryCPU
    {
        [Required(ErrorMessage = "Название процессора не установлено")]
        public string ProcessorName { get; set; }
        public DateTime ReleaseDate { get; set; } // год выхода
        public string Socket { get; set; } // сокет
        public string Series { get; set; } // серия
        public int Cores { get; set; } // количество ядер
        public int Threads { get; set; } // количество потоков
        public bool IGPU { get; set; } // поддержка многопоточности
        public double ClockFrequency { get; set; } // тактовая частота
        public int Techprocess { get; set; } // техпроцесс
        public int TDP { get; set; } // техпроцесс

        // Конструкторы
        public Processor(string companyName, string category) :
            base(companyName, category)
        {

        }
        public Processor(string companyName, DateTime creationDate, string category, DateTime releaseDate, string socket,
            string series, int cores, int threads, bool smt, double frequency, int techprocess, int tdp, string processorName) :
            base(companyName, creationDate, category)
        {
            ReleaseDate = releaseDate;
            Socket = socket;
            Series = series;
            Cores = cores;
            Threads = threads;
            IGPU = smt;
            ClockFrequency = frequency;
            Techprocess = techprocess;
            TDP = tdp;
            ProcessorName = processorName;
        }
        // Методы
        /// <summary>
        /// Переопределение метода Equals. Сравнение объектов происходит по значению поля <c>ProcessorName</c>
        /// </summary>
        /// <param name="obj">Объект для сравнения</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            Processor processor = (Processor)obj;
            return processor.ProcessorName == ProcessorName;
        }
        public override int GetHashCode()
        {
            // Переопределение метода Equals
            return ProcessorName.GetHashCode();
        }
        public override int CompareTo(object o)
        {
            Processor category = o as Processor;
            if (category != null)
                return ProcessorName.CompareTo(category.ProcessorName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }
    }
    public static class Companies
    {
        public static List<Company> Items { get; set; }
        // События
        public static event AddItem ItemAdded;
        // Методы
        public static void AddCompany(Company company)
        {
            Items.Add(company);
            ItemAdded?.Invoke(company);
        }
        public static void RemoveCompany(Company company)
        {
            Items.Remove(company);
        }
        /// <summary>
        /// Проверяет существует ли элемент в Items с CompanyName = name, если да, то возвращает true, иначе false.
        /// </summary>
        /// <param name="name">Название компании</param>
        /// <returns></returns>
        public static bool FindItem(string name)
        {
            bool bret = false;

            foreach (Company item in Items)
            {
                if (item.CompanyName == name)
                {
                    bret = true;
                    break;
                }
            }

            return bret;
        }
        /// <summary>
        /// Получает элемент по имени
        /// </summary>
        /// <param name="name">Название компании</param>
        /// <returns></returns>
        public static Company GetCompany(string name)
        {
            return Items.Find(item => string.Equals(item.CompanyName, name));
        }
        /// <summary>
        /// Очищает <c>comboBox</c> и добавляет данные из списка в <c>comboBox</c>.
        /// В список устанавливается значение поля <c>CompanyName</c>
        /// </summary>
        /// <param name="comboBox"><c>ComboBox</c>, в который нужно добавить данные.</param>
        public static void SetDataToComboBox(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            if (Items.Count > 0)
            {
                foreach (Company item in Items)
                {
                    comboBox.Items.Add(item.CompanyName);
                }
                comboBox.SelectedIndex = 0;
            }
        }
    }
    public static class Categories
    {
        public static List<CategoryCPU> Items { get; set; }
        // События
        public static event AddItem ItemAdded;
        // Методы
        public static void AddCategoryCPU(CategoryCPU category)
        {
            Items.Add(category);
            ItemAdded?.Invoke(category);
        }
        public static void RemoveCategoryCPU(CategoryCPU category)
        {
            Items.Remove(category);
        }
        /// <summary>
        /// Ищет элемент в списке с именем <c>categoryName</c>, который принадлежат компании <c>companyName</c>,
        /// и возвращает true, если элемент был найден, иначе false.
        /// </summary>
        /// <param name="companyName">Название компании</param>
        /// <param name="categoryName">Название категории</param>
        /// <returns></returns>
        public static bool FindItem(string companyName, string categoryName)
        {
            bool bret = false;

            foreach (CategoryCPU item in Items)
            {
                if ((item.CompanyName == companyName) && (item.Category == categoryName))
                {
                    bret = true;
                    break;
                }
            }

            return bret;
        }
        /// <summary>
        /// Получает элемент по имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static CategoryCPU GetCategoryCPU(string name)
        {
            return Items.Find(item => item.Category == name);
        }
        /// <summary>
        /// Получает список категорий, которые принадлежат компании <c>companyName</c>.
        /// </summary>
        /// <param name="companyName">Название компании</param>
        /// <returns>Возвращает список <c>List<CategoryCPU></c></returns>
        public static List<CategoryCPU> GetCategoriesOfCompany(string companyName)
        {
            return Items.FindAll(item => item.CompanyName == companyName);
        }
        /// <summary>
        /// Очищает <c>comboBox</c> и добавляет данные из списка в <c>comboBox</c>.
        /// В список устанавливается значение поля <c>Category</c> экземпялара класса <c>CategoryCPU</c>,
        /// где поле <c>CompanyName</c> совпадает с входным параметром <c>companyName</c>.
        /// </summary>
        /// <param name="comboBox"><c>ComboBox</c>, в который нужно добавить данные.</param>
        /// <param name="companyName">Название компании, категории которой нужно вывести в <c>ComboBox</c></param>
        public static void SetDataToComboBox(ComboBox comboBox, string companyName)
        {
            comboBox.Items.Clear();
            if (Items.Count > 0)
            {
                foreach (CategoryCPU item in Items)
                {
                    if (item.CompanyName == companyName)
                    {
                        comboBox.Items.Add(item.Category);
                    }
                }

                if (comboBox.Items.Count > 0)
                {
                    comboBox.SelectedIndex = 0;
                }
            }
        }
    }
    public static class Processors
    {
        public static List<Processor> Items { get; set; }
        // События
        public static event AddItem ItemAdded;
        // Методы
        /// <summary>
        /// Копирует значения полей из <c>newProc</c> в <c>proc</c>
        /// </summary>
        /// <param name="newProc">Экземпляр объекта откуда надо скопировать значения</param>
        /// <param name="proc">Экземпляр объекта куда надо скопировать значения</param>
        public static void CopyProcessor(Processor newProc, Processor proc)
        {
            proc.CompanyName = newProc.CompanyName;
            proc.Category = newProc.Category;
            proc.ReleaseDate = newProc.ReleaseDate;
            proc.ProcessorName = newProc.ProcessorName;
            proc.Socket = newProc.Socket;
            proc.Series = newProc.Series;
            proc.Cores = newProc.Cores;
            proc.Threads = newProc.Threads;
            proc.IGPU = newProc.IGPU;
            proc.ClockFrequency = newProc.ClockFrequency;
            proc.Techprocess = newProc.Techprocess;
            proc.TDP = newProc.TDP;
        }
        public static void AddProcessor(Processor processor)
        {
            Items.Add(processor);
            ItemAdded?.Invoke(processor);
        }
        public static void RemoveProcessor(Processor processor)
        {
            Items.Remove(processor);
        }
        /// <summary>
        /// Ищет элемент в списке и возвращает true, если элемент найден, иначе false.
        /// </summary>
        /// <param name="processor"></param>
        /// <returns></returns>
        public static bool FindItem(Processor processor)
        {
            bool bret = false;

            foreach (Processor item in Items)
            {
                if (item.Equals(processor))
                {
                    bret = true;
                    break;
                }
            }

            return bret;
        }
        /// <summary>
        /// Получает элемент по имени
        /// </summary>
        /// <param name="name">Название процессора</param>
        /// <returns></returns>
        public static Processor GetProcessor(string name)
        {
            return Items.Find(item => item.ProcessorName == name);
        }
        /// <summary>
        /// Получает список процессоров, которые принадлежат компании <c>companyName</c>
        /// и находятся в категории <c>category</c>.
        /// </summary>
        /// <param name="companyName">Название компании</param>
        /// <param name="category">Название категории</param>
        /// <returns>Возвращает список <c>List<Processor></c></returns>
        public static List<Processor> GetProcessorsOfCompanyCategory(string companyName, string category)
        {
            return Items.FindAll(item => (item.CompanyName == companyName) && (item.Category == category));
        }
    }
    public static class MySerializer
    {
        /// <summary>
        /// Сериализует данные. Создаёт 1 xml файл, который содержит следующую структуру. <c>DATA</c> с элементами <c>company</c>,
        /// в которых содержатся элементы <c>category</c>, в которых содержатся элементы <c>processor</c>
        /// </summary>
        public static void SaveToXml(string path)
        {
            XDocument xDocument = new XDocument();
            XElement xElement = new XElement("DATA");

            if (Companies.Items.Count > 0)
            {
                XElement companies = new XElement("companies");
                foreach (Company comp in Companies.Items)
                {
                    XElement company = new XElement("company");

                    company.Add(new XAttribute("name", comp.CompanyName));
                    company.Add(new XElement("CreationDate", comp.CreationDate));

                    companies.Add(company);
                }
                xElement.Add(companies);
            }

            if (Categories.Items.Count > 0)
            {
                XElement categories = new XElement("categories");
                foreach (CategoryCPU cat in Categories.Items)
                {
                    XElement category = new XElement("category");

                    category.Add(new XAttribute("name", cat.Category));
                    category.Add(new XElement("CompanyName", cat.CompanyName));
                    category.Add(new XElement("CreationDate", cat.CreationDate));

                    categories.Add(category);
                }
                xElement.Add(categories);
            }

            if (Processors.Items.Count > 0)
            {
                XElement processors = new XElement("processors");
                foreach (Processor proc in Processors.Items)
                {
                    XElement processor = new XElement("processor");

                    processor.Add(new XAttribute("name", proc.ProcessorName));
                    processor.Add(new XElement("CompanyName", proc.CompanyName));
                    processor.Add(new XElement("Category", proc.Category));
                    processor.Add(new XElement("CreationDate", proc.CreationDate));
                    processor.Add(new XElement("ReleaseDate", proc.ReleaseDate));
                    processor.Add(new XElement("Socket", proc.Socket));
                    processor.Add(new XElement("Series", proc.Series));
                    processor.Add(new XElement("Cores", proc.Cores));
                    processor.Add(new XElement("Threads", proc.Threads));
                    processor.Add(new XElement("IGPU", proc.IGPU));
                    processor.Add(new XElement("ClockFrequency", proc.ClockFrequency));
                    processor.Add(new XElement("Techprocess", proc.Techprocess));
                    processor.Add(new XElement("TDP", proc.TDP));

                    processors.Add(processor);
                }
                xElement.Add(processors);
            }

            xDocument.Add(xElement);
            xDocument.Save(path);
        }
        /// <summary>
        /// Десериализует данные.
        /// </summary>
        public static void LoadFromXml(string path)
        {
            XmlDocument document = new XmlDocument();
            document.Load(path);

            XmlElement xRoot = document.DocumentElement;

            if (xRoot.HasChildNodes)
            {
                foreach (XmlElement xnode in xRoot)
                {
                    if (xnode.Name == "companies")
                    {
                        if (xnode.HasChildNodes)
                        {
                            DeserializeCompanies(xnode.ChildNodes);
                        }
                    }
                    if (xnode.Name == "categories")
                    {
                        if (xnode.HasChildNodes)
                        {
                            DeserializeCategories(xnode.ChildNodes);
                        }
                    }
                    if (xnode.Name == "processors")
                    {
                        if (xnode.HasChildNodes)
                        {
                            DeserializeProcessors(xnode.ChildNodes);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Десериализует данные из <c>nodeList</c>. Если произошла ошибка, то генерирует исключение.
        /// </summary>
        /// <param name="nodeList">Список <c>XmlNodeList</c> с информацией о компаниях</param>
        private static void DeserializeCompanies(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    XmlNode name = null;
                    DateTime date = DateTime.Now;

                    if (node.Attributes.Count > 0)
                    {
                        name = node.Attributes.GetNamedItem("name");
                    }

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "CreationDate")
                        {
                            date = DateTime.Parse(childNode.InnerText);
                        }
                    }

                    Company company = new Company(name.InnerText, date);
                    Companies.AddCompany(company);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о компании, возможно файл был повреждён");
            }
        }
        /// <summary>
        /// Десериализует данные из <c>nodeList</c>. Если произошла ошибка, то генерирует исключение.
        /// </summary>
        /// <param name="nodeList">Список <c>XmlNodeList</c> с информацией о категориях</param>
        private static void DeserializeCategories(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    XmlNode name = null;
                    DateTime date = DateTime.Now;
                    string companyName = null;

                    if (node.Attributes.Count > 0)
                    {
                        name = node.Attributes.GetNamedItem("name");
                    }

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "CreationDate")
                        {
                            date = DateTime.Parse(childNode.InnerText);
                        }
                        if (childNode.Name == "CompanyName")
                        {
                            companyName = childNode.InnerText;
                        }
                    }

                    CategoryCPU category = new CategoryCPU(companyName, date, name.InnerText);
                    Categories.AddCategoryCPU(category);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о категории, возможно файл был повреждён");
            }
        }
        /// <summary>
        /// Десериализует данные из <c>nodeList</c>. Если произошла ошибка, то генерирует исключение.
        /// </summary>
        /// <param name="nodeList">Список <c>XmlNodeList</c> с информацией о процессорах</param>
        private static void DeserializeProcessors(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    XmlNode name = null;
                    DateTime date = DateTime.Now;
                    DateTime dateRelease = DateTime.Now;
                    string companyName = null;
                    string category = null;
                    string socket = null;
                    string series = null;
                    int cores = 0, threads = 0, tdp = 0, techprocess = 0;
                    bool igpu = false;
                    double frequency = 0;

                    if (node.Attributes.Count > 0)
                    {
                        name = node.Attributes.GetNamedItem("name");
                    }

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "CreationDate")
                        {
                            date = DateTime.Parse(childNode.InnerText);
                        }
                        if (childNode.Name == "CompanyName")
                        {
                            companyName = childNode.InnerText;
                        }
                        if (childNode.Name == "Category")
                        {
                            category = childNode.InnerText;
                        }
                        if (childNode.Name == "ReleaseDate")
                        {
                            dateRelease = DateTime.Parse(childNode.InnerText);
                        }
                        if (childNode.Name == "Socket")
                        {
                            socket = childNode.InnerText;
                        }
                        if (childNode.Name == "Series")
                        {
                            series = childNode.InnerText;
                        }
                        if (childNode.Name == "Cores")
                        {
                            int.TryParse(childNode.InnerText, out cores);
                        }
                        if (childNode.Name == "Threads")
                        {
                            int.TryParse(childNode.InnerText, out threads);
                        }
                        if (childNode.Name == "Techprocess")
                        {
                            int.TryParse(childNode.InnerText, out techprocess);
                        }
                        if (childNode.Name == "TDP")
                        {
                            int.TryParse(childNode.InnerText, out tdp);
                        }
                        if (childNode.Name == "IGPU")
                        {
                            igpu = bool.Parse(childNode.InnerText);
                        }
                        if (childNode.Name == "ClockFrequency")
                        {
                            double.TryParse(childNode.InnerText, out frequency);
                        }
                    }

                    Processor processor = new Processor(companyName, date, category, dateRelease, socket, series, cores,
                                                 threads, igpu, frequency, techprocess, tdp, name.InnerText);
                    Processors.AddProcessor(processor);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о процессоре, возможно файл был повреждён");
            }
        }
    }
}