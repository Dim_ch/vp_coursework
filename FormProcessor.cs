﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormProcessor : Form
    {
        public Processor NewProcessor;
        public Processor OldProcessor;
        private bool FlagEdit = false;
        // Конструкторы.
        public FormProcessor()
        {
            InitializeComponent();
            Companies.SetDataToComboBox(ComboBox_Company);
            if (ComboBox_Company.Items.Count > 0)
            {
                Categories.SetDataToComboBox(ComboBox_Category, ComboBox_Company.SelectedItem.ToString());
            }
        }
        public FormProcessor(Processor processor)
        {
            InitializeComponent();

            OldProcessor = processor;
            Companies.SetDataToComboBox(ComboBox_Company);

            if (ComboBox_Company.Items.Count > 0)
            {
                Categories.SetDataToComboBox(ComboBox_Category, ComboBox_Company.SelectedItem.ToString());
            }

            ComboBox_Category.SelectedItem = processor.Category;
            ComboBox_Company.SelectedItem = processor.CompanyName;
            CheckBox_iGPU.Checked = processor.IGPU;
            TextBox_Frequency.Text = processor.ClockFrequency.ToString();
            TextBox_ProcessorName.Text = processor.ProcessorName;
            TextBox_Series.Text = processor.Series;
            TextBox_Socket.Text = processor.Socket;
            DTM_Release_Date.Value = processor.ReleaseDate;
            NumericUpDown_TDP.Value = processor.TDP;
            NumericUpDown_Cores.Value = processor.Cores;
            NumericUpDown_Threads.Value = processor.Threads;
            NumericUpDown_Techprocess.Value = processor.Techprocess;

            ComboBox_Category.Enabled = false;
            ComboBox_Company.Enabled = false;
            Button_Add_Company.Enabled = false;
            Button_Add_Category.Enabled = false;
            FlagEdit = true;
        }
        // Методы.
        /// <summary>Метод ValidateForm.
        /// <para>Проверяет поля формы. Если все данные введены корректно возвращает <c>true</c></para>
        /// <param name="processor">Получает новый объект типа <c>Processor</c>, когда метод возвращает true</param>
        /// </summary>
        private bool ValidateForm(out Processor processor)
        {
            processor = null;
            if (ComboBox_Company.Items.Count == 0 || ComboBox_Category.Items.Count == 0)
            {
                _ = MessageBox.Show("Убедитесь, что выбраны компания и категория");
                return false;
            }
            string company = ComboBox_Company.SelectedItem.ToString();
            string category = ComboBox_Category.SelectedItem.ToString();
            string socket = TextBox_Socket.Text;
            string series = TextBox_Series.Text;
            string processorName = TextBox_ProcessorName.Text;
            DateTime releaseDate = DTM_Release_Date.Value;
            bool igpu = CheckBox_iGPU.Checked;

            if (Double.TryParse(TextBox_Frequency.Text, out double frequency) == false)
            {
                _ = MessageBox.Show("Поле 'Частота' должно содержать десятичное число");
                return false;
            }
            else if (frequency == 0)
            {
                _ = MessageBox.Show("Поле 'Частота' должно быть больше 0");
                return false;
            }

            if (int.TryParse(NumericUpDown_TDP.Value.ToString(), out int tdp) == false)
            {
                _ = MessageBox.Show("Поле 'Тепловыделение' должно содержать целое число");
                return false;

            }
            else if (tdp == 0)
            {
                _ = MessageBox.Show("Поле 'Тепловыделение' должно быть больше 0");
                return false;
            }

            if (int.TryParse(NumericUpDown_Cores.Value.ToString(), out int cores) == false)
            {
                _ = MessageBox.Show("Поле 'Количество ядер' должно содержать целое число");
                return false;

            }
            else if (cores == 0)
            {
                _ = MessageBox.Show("Поле 'Количество ядер' должно быть больше 0");
                return false;
            }

            if (int.TryParse(NumericUpDown_Threads.Value.ToString(), out int threads) == false)
            {
                _ = MessageBox.Show("Поле 'Количество потоков' должно содержать целое число");
                return false;

            }
            else if (threads == 0)
            {
                _ = MessageBox.Show("Поле 'Количество потоков' должно быть больше 0");
                return false;
            }

            if (int.TryParse(NumericUpDown_Techprocess.Value.ToString(), out int techprocess) == false)
            {
                _ = MessageBox.Show("Поле 'Техпроцесс' должно содержать целое число");
                return false;
            }
            else if (techprocess == 0)
            {
                _ = MessageBox.Show("Поле 'Техпроцесс' должно быть больше 0");
                return false;
            }

            if (String.IsNullOrWhiteSpace(socket))
            {
                _ = MessageBox.Show("Введите сокет");
                return false;
            }

            if (String.IsNullOrWhiteSpace(series))
            {
                _ = MessageBox.Show("Введите серию");
                return false;
            }

            if (String.IsNullOrWhiteSpace(company))
            {
                _ = MessageBox.Show("Выберите компанию");
                return false;
            }

            if (String.IsNullOrWhiteSpace(category))
            {
                _ = MessageBox.Show("Выберите категорию");
                return false;
            }

            if (String.IsNullOrWhiteSpace(processorName))
            {
                _ = MessageBox.Show("Введите название процессора");
                return false;
            }

            Processor processor1 = new Processor(company, DateTime.Now, category, releaseDate, socket, series, cores,
                                                 threads, igpu, frequency, techprocess, tdp, processorName);

            bool flag = false;
            if (FlagEdit)
            {
                if (OldProcessor.ProcessorName != TextBox_ProcessorName.Text)
                {
                    flag = Processors.FindItem(processor1);
                }
            }
            else
            {
                flag = Processors.FindItem(processor1);
            }

            if (flag)
            {
                _ = MessageBox.Show("Процессор с таким именем уже существует");
                return false;
            }

            processor = processor1;
            return true;
        }
        // Обработчики.
        private void Button_Add_Company_Click(object sender, EventArgs e)
        {
            FormCompany form = new FormCompany();

            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                int iIndex = ComboBox_Company.Items.Add(form.TextBox_Company.Text);
                ComboBox_Company.SelectedIndex = iIndex;

                Companies.AddCompany(form.NewCompany);
            }
        }

        private void Button_Add_Category_Click(object sender, EventArgs e)
        {
            if (Companies.Items.Count == 0)
            {
                _ = MessageBox.Show("Добавьте компанию");
                return;
            }

            FormCategory form = new FormCategory(ComboBox_Company.SelectedItem.ToString());

            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                Categories.AddCategoryCPU(form.NewCategory);
                if (ComboBox_Company.SelectedItem.ToString() == form.NewCategory.CompanyName)
                {
                    // Если выбранная компания совпадает с form.NewCategory.CompanyName, то добавить в ComboBox_Category элемент
                    int iIndex = ComboBox_Category.Items.Add(form.NewCategory.Category);
                    ComboBox_Category.SelectedIndex = iIndex;
                }
            }
        }

        private void Button_Apply_Click(object sender, EventArgs e)
        {
            if (ValidateForm(out NewProcessor))
            {
                DialogResult = DialogResult.OK;
            }

        }

        private void ComboBox_Company_SelectedIndexChanged(object sender, EventArgs e)
        {
            Categories.SetDataToComboBox(ComboBox_Category, ComboBox_Company.SelectedItem.ToString());
        }

    }
}
