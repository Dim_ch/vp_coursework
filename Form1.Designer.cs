﻿namespace CourseWork
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Data = new System.Windows.Forms.TreeView();
            this.Button_AddCompany = new System.Windows.Forms.Button();
            this.Button_Delete = new System.Windows.Forms.Button();
            this.Button_Edit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Label_IGPU = new System.Windows.Forms.Label();
            this.Label_ProcessorName = new System.Windows.Forms.Label();
            this.Label_Frequency = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Label_Techprocess = new System.Windows.Forms.Label();
            this.Label_Socket = new System.Windows.Forms.Label();
            this.Label_Series = new System.Windows.Forms.Label();
            this.Label_Cores = new System.Windows.Forms.Label();
            this.Label_Threads = new System.Windows.Forms.Label();
            this.Label_TDP = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Label_ReleaseDate = new System.Windows.Forms.Label();
            this.Label_Category = new System.Windows.Forms.Label();
            this.Label_Company = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Button_AddProcessor = new System.Windows.Forms.Button();
            this.Button_AddCategory = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Button_SortProcessor = new System.Windows.Forms.Button();
            this.Button_SortCategory = new System.Windows.Forms.Button();
            this.Button_SortCompany = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.userControl21 = new CourseWork.LongestWord();
            this.userControl11 = new CourseWork.ConverterLength();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.MenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFile});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(799, 24);
            this.MenuStrip.TabIndex = 0;
            this.MenuStrip.Text = "menuStrip2";
            // 
            // MenuFile
            // 
            this.MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemSave,
            this.MenuItemLoad});
            this.MenuFile.Name = "MenuFile";
            this.MenuFile.Size = new System.Drawing.Size(48, 20);
            this.MenuFile.Text = "Файл";
            // 
            // MenuItemSave
            // 
            this.MenuItemSave.Name = "MenuItemSave";
            this.MenuItemSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.MenuItemSave.Size = new System.Drawing.Size(173, 22);
            this.MenuItemSave.Text = "Сохранить";
            // 
            // MenuItemLoad
            // 
            this.MenuItemLoad.Name = "MenuItemLoad";
            this.MenuItemLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.MenuItemLoad.Size = new System.Drawing.Size(173, 22);
            this.MenuItemLoad.Text = "Загрузить";
            // 
            // TreeView_Data
            // 
            this.TreeView_Data.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeView_Data.Location = new System.Drawing.Point(3, 3);
            this.TreeView_Data.Name = "TreeView_Data";
            this.TreeView_Data.Size = new System.Drawing.Size(415, 423);
            this.TreeView_Data.TabIndex = 10;
            this.TreeView_Data.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView_Data_AfterSelect);
            // 
            // Button_AddCompany
            // 
            this.Button_AddCompany.Location = new System.Drawing.Point(6, 22);
            this.Button_AddCompany.Name = "Button_AddCompany";
            this.Button_AddCompany.Size = new System.Drawing.Size(105, 23);
            this.Button_AddCompany.TabIndex = 0;
            this.Button_AddCompany.Text = "Компанию";
            this.Button_AddCompany.UseVisualStyleBackColor = true;
            this.Button_AddCompany.Click += new System.EventHandler(this.Button_AddCompany_Click);
            // 
            // Button_Delete
            // 
            this.Button_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Delete.Location = new System.Drawing.Point(610, 401);
            this.Button_Delete.Name = "Button_Delete";
            this.Button_Delete.Size = new System.Drawing.Size(178, 23);
            this.Button_Delete.TabIndex = 4;
            this.Button_Delete.Text = "Удалить";
            this.Button_Delete.UseVisualStyleBackColor = true;
            this.Button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // Button_Edit
            // 
            this.Button_Edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Edit.Location = new System.Drawing.Point(424, 401);
            this.Button_Edit.Name = "Button_Edit";
            this.Button_Edit.Size = new System.Drawing.Size(180, 23);
            this.Button_Edit.TabIndex = 3;
            this.Button_Edit.Text = "Редактировать";
            this.Button_Edit.UseVisualStyleBackColor = true;
            this.Button_Edit.Click += new System.EventHandler(this.Button_Edit_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Компания:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Категория:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата выпуска:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(99, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Сокет:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Тепловыделение:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Количество ядер:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Количество потоков:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(97, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Серия:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 202);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Техпроцесс в нм:";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(57, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Частота в ГГц:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.Label_IGPU);
            this.groupBox1.Controls.Add(this.Label_ProcessorName);
            this.groupBox1.Controls.Add(this.Label_Frequency);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.Label_Techprocess);
            this.groupBox1.Controls.Add(this.Label_Socket);
            this.groupBox1.Controls.Add(this.Label_Series);
            this.groupBox1.Controls.Add(this.Label_Cores);
            this.groupBox1.Controls.Add(this.Label_Threads);
            this.groupBox1.Controls.Add(this.Label_TDP);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Label_ReleaseDate);
            this.groupBox1.Controls.Add(this.Label_Category);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Label_Company);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(424, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 270);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация о процессоре";
            // 
            // Label_IGPU
            // 
            this.Label_IGPU.AutoSize = true;
            this.Label_IGPU.Location = new System.Drawing.Point(147, 242);
            this.Label_IGPU.Name = "Label_IGPU";
            this.Label_IGPU.Size = new System.Drawing.Size(62, 15);
            this.Label_IGPU.TabIndex = 26;
            this.Label_IGPU.Text = "Не задано";
            // 
            // Label_ProcessorName
            // 
            this.Label_ProcessorName.AutoSize = true;
            this.Label_ProcessorName.Location = new System.Drawing.Point(147, 62);
            this.Label_ProcessorName.Name = "Label_ProcessorName";
            this.Label_ProcessorName.Size = new System.Drawing.Size(62, 15);
            this.Label_ProcessorName.TabIndex = 17;
            this.Label_ProcessorName.Text = "Не задано";
            // 
            // Label_Frequency
            // 
            this.Label_Frequency.AutoSize = true;
            this.Label_Frequency.Location = new System.Drawing.Point(147, 222);
            this.Label_Frequency.Name = "Label_Frequency";
            this.Label_Frequency.Size = new System.Drawing.Size(62, 15);
            this.Label_Frequency.TabIndex = 25;
            this.Label_Frequency.Text = "Не задано";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(97, 242);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 15);
            this.label12.TabIndex = 14;
            this.label12.Text = "? iGPU:";
            // 
            // Label_Techprocess
            // 
            this.Label_Techprocess.AutoSize = true;
            this.Label_Techprocess.Location = new System.Drawing.Point(147, 202);
            this.Label_Techprocess.Name = "Label_Techprocess";
            this.Label_Techprocess.Size = new System.Drawing.Size(62, 15);
            this.Label_Techprocess.TabIndex = 24;
            this.Label_Techprocess.Text = "Не задано";
            // 
            // Label_Socket
            // 
            this.Label_Socket.AutoSize = true;
            this.Label_Socket.Location = new System.Drawing.Point(147, 102);
            this.Label_Socket.Name = "Label_Socket";
            this.Label_Socket.Size = new System.Drawing.Size(62, 15);
            this.Label_Socket.TabIndex = 19;
            this.Label_Socket.Text = "Не задано";
            // 
            // Label_Series
            // 
            this.Label_Series.AutoSize = true;
            this.Label_Series.Location = new System.Drawing.Point(147, 182);
            this.Label_Series.Name = "Label_Series";
            this.Label_Series.Size = new System.Drawing.Size(62, 15);
            this.Label_Series.TabIndex = 23;
            this.Label_Series.Text = "Не задано";
            // 
            // Label_Cores
            // 
            this.Label_Cores.AutoSize = true;
            this.Label_Cores.Location = new System.Drawing.Point(147, 142);
            this.Label_Cores.Name = "Label_Cores";
            this.Label_Cores.Size = new System.Drawing.Size(62, 15);
            this.Label_Cores.TabIndex = 21;
            this.Label_Cores.Text = "Не задано";
            // 
            // Label_Threads
            // 
            this.Label_Threads.AutoSize = true;
            this.Label_Threads.Location = new System.Drawing.Point(147, 162);
            this.Label_Threads.Name = "Label_Threads";
            this.Label_Threads.Size = new System.Drawing.Size(62, 15);
            this.Label_Threads.TabIndex = 22;
            this.Label_Threads.Text = "Не задано";
            // 
            // Label_TDP
            // 
            this.Label_TDP.AutoSize = true;
            this.Label_TDP.Location = new System.Drawing.Point(147, 122);
            this.Label_TDP.Name = "Label_TDP";
            this.Label_TDP.Size = new System.Drawing.Size(62, 15);
            this.Label_TDP.TabIndex = 20;
            this.Label_TDP.Text = "Не задано";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(79, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 13;
            this.label11.Text = "Название:";
            // 
            // Label_ReleaseDate
            // 
            this.Label_ReleaseDate.AutoSize = true;
            this.Label_ReleaseDate.Location = new System.Drawing.Point(147, 82);
            this.Label_ReleaseDate.Name = "Label_ReleaseDate";
            this.Label_ReleaseDate.Size = new System.Drawing.Size(62, 15);
            this.Label_ReleaseDate.TabIndex = 18;
            this.Label_ReleaseDate.Text = "Не задано";
            // 
            // Label_Category
            // 
            this.Label_Category.AutoSize = true;
            this.Label_Category.Location = new System.Drawing.Point(147, 42);
            this.Label_Category.Name = "Label_Category";
            this.Label_Category.Size = new System.Drawing.Size(62, 15);
            this.Label_Category.TabIndex = 16;
            this.Label_Category.Text = "Не задано";
            // 
            // Label_Company
            // 
            this.Label_Company.AutoSize = true;
            this.Label_Company.Location = new System.Drawing.Point(147, 22);
            this.Label_Company.Name = "Label_Company";
            this.Label_Company.Size = new System.Drawing.Size(62, 15);
            this.Label_Company.TabIndex = 15;
            this.Label_Company.Text = "Не задано";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.Button_AddProcessor);
            this.groupBox2.Controls.Add(this.Button_AddCategory);
            this.groupBox2.Controls.Add(this.Button_AddCompany);
            this.groupBox2.Location = new System.Drawing.Point(424, 279);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 54);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Добавить:";
            // 
            // Button_AddProcessor
            // 
            this.Button_AddProcessor.Location = new System.Drawing.Point(231, 22);
            this.Button_AddProcessor.Name = "Button_AddProcessor";
            this.Button_AddProcessor.Size = new System.Drawing.Size(127, 23);
            this.Button_AddProcessor.TabIndex = 2;
            this.Button_AddProcessor.Text = "Процессор";
            this.Button_AddProcessor.UseVisualStyleBackColor = true;
            this.Button_AddProcessor.Click += new System.EventHandler(this.Button_AddProcessor_Click);
            // 
            // Button_AddCategory
            // 
            this.Button_AddCategory.Location = new System.Drawing.Point(117, 22);
            this.Button_AddCategory.Name = "Button_AddCategory";
            this.Button_AddCategory.Size = new System.Drawing.Size(108, 23);
            this.Button_AddCategory.TabIndex = 1;
            this.Button_AddCategory.Text = "Категорию";
            this.Button_AddCategory.UseVisualStyleBackColor = true;
            this.Button_AddCategory.Click += new System.EventHandler(this.Button_AddCategory_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.Button_SortProcessor);
            this.groupBox3.Controls.Add(this.Button_SortCategory);
            this.groupBox3.Controls.Add(this.Button_SortCompany);
            this.groupBox3.Location = new System.Drawing.Point(424, 339);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(363, 56);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Соритровать:";
            // 
            // Button_SortProcessor
            // 
            this.Button_SortProcessor.Location = new System.Drawing.Point(231, 22);
            this.Button_SortProcessor.Name = "Button_SortProcessor";
            this.Button_SortProcessor.Size = new System.Drawing.Size(126, 23);
            this.Button_SortProcessor.TabIndex = 5;
            this.Button_SortProcessor.Text = "Процессоры";
            this.Button_SortProcessor.UseVisualStyleBackColor = true;
            this.Button_SortProcessor.Click += new System.EventHandler(this.Button_SortProcessor_Click);
            // 
            // Button_SortCategory
            // 
            this.Button_SortCategory.Location = new System.Drawing.Point(117, 22);
            this.Button_SortCategory.Name = "Button_SortCategory";
            this.Button_SortCategory.Size = new System.Drawing.Size(108, 23);
            this.Button_SortCategory.TabIndex = 4;
            this.Button_SortCategory.Text = "Категории";
            this.Button_SortCategory.UseVisualStyleBackColor = true;
            this.Button_SortCategory.Click += new System.EventHandler(this.Button_SortCategory_Click);
            // 
            // Button_SortCompany
            // 
            this.Button_SortCompany.Location = new System.Drawing.Point(6, 22);
            this.Button_SortCompany.Name = "Button_SortCompany";
            this.Button_SortCompany.Size = new System.Drawing.Size(105, 23);
            this.Button_SortCompany.TabIndex = 3;
            this.Button_SortCompany.Text = "Компании";
            this.Button_SortCompany.UseVisualStyleBackColor = true;
            this.Button_SortCompany.Click += new System.EventHandler(this.Button_SortCompany_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(799, 461);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TreeView_Data);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.Button_Delete);
            this.tabPage1.Controls.Add(this.Button_Edit);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(791, 433);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Главная";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.userControl21);
            this.tabPage2.Controls.Add(this.userControl11);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 433);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Элементы управления";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // userControl21
            // 
            this.userControl21.AllowDrop = true;
            this.userControl21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControl21.Location = new System.Drawing.Point(252, 4);
            this.userControl21.Name = "userControl21";
            this.userControl21.Size = new System.Drawing.Size(284, 95);
            this.userControl21.TabIndex = 1;
            // 
            // userControl11
            // 
            this.userControl11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControl11.Location = new System.Drawing.Point(3, 4);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(237, 53);
            this.userControl11.TabIndex = 0;
            // 
            // toolTip1
            // 
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Подсказка";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 485);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.MenuStrip);
            this.MainMenuStrip = this.MenuStrip;
            this.MinimumSize = new System.Drawing.Size(815, 524);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Курсовой проект";
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuFile;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSave;
        private System.Windows.Forms.TreeView TreeView_Data;
        private System.Windows.Forms.Button Button_AddCompany;
        private System.Windows.Forms.Button Button_Delete;
        private System.Windows.Forms.Button Button_Edit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Button_AddCategory;
        private System.Windows.Forms.Button Button_AddProcessor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Label_Frequency;
        private System.Windows.Forms.Label Label_Techprocess;
        private System.Windows.Forms.Label Label_Series;
        private System.Windows.Forms.Label Label_Threads;
        private System.Windows.Forms.Label Label_Cores;
        private System.Windows.Forms.Label Label_TDP;
        private System.Windows.Forms.Label Label_Socket;
        private System.Windows.Forms.Label Label_ReleaseDate;
        private System.Windows.Forms.Label Label_Category;
        private System.Windows.Forms.Label Label_Company;
        private System.Windows.Forms.Label Label_ProcessorName;
        private System.Windows.Forms.Label Label_IGPU;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Button_SortProcessor;
        private System.Windows.Forms.Button Button_SortCategory;
        private System.Windows.Forms.Button Button_SortCompany;
        private System.Windows.Forms.ToolStripMenuItem MenuItemLoad;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private ConverterLength userControl11;
        private LongestWord userControl21;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

