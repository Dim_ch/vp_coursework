﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormCategory : Form
    {
        public CategoryCPU NewCategory;
        public FormCategory()
        {
            InitializeComponent();
            Companies.SetDataToComboBox(ComboBox_Company);
        }
        public FormCategory(string companyName)
        {
            InitializeComponent();
            Companies.SetDataToComboBox(ComboBox_Company);
            ComboBox_Company.SelectedItem = companyName;
        }
        public FormCategory(CategoryCPU category)
        {
            InitializeComponent();
            Companies.SetDataToComboBox(ComboBox_Company);
            ComboBox_Company.SelectedItem = category.CompanyName;
            ComboBox_Company.Enabled = false;
            TextBox_Category.Text = category.Category;
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TextBox_Category.Text))
            {
                _ = MessageBox.Show("Введите название");
            }
            else
            {
                if (Categories.FindItem(ComboBox_Company.SelectedItem.ToString(), TextBox_Category.Text))
                {
                    string message = string.Format("Категория {0} с таким именем уже существует в компании {1}",
                        TextBox_Category.Text, ComboBox_Company.SelectedItem.ToString());
                    _ = MessageBox.Show(message);
                }
                else
                {
                    NewCategory = new CategoryCPU(ComboBox_Company.SelectedItem.ToString(), TextBox_Category.Text);
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
