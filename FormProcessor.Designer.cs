﻿
namespace CourseWork
{
    partial class FormProcessor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProcessor));
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TextBox_Socket = new System.Windows.Forms.TextBox();
            this.TextBox_Frequency = new System.Windows.Forms.TextBox();
            this.TextBox_Series = new System.Windows.Forms.TextBox();
            this.CheckBox_iGPU = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBox_ProcessorName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NumericUpDown_TDP = new System.Windows.Forms.NumericUpDown();
            this.ComboBox_Category = new System.Windows.Forms.ComboBox();
            this.ComboBox_Company = new System.Windows.Forms.ComboBox();
            this.Button_Add_Category = new System.Windows.Forms.Button();
            this.Button_Add_Company = new System.Windows.Forms.Button();
            this.NumericUpDown_Techprocess = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown_Threads = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown_Cores = new System.Windows.Forms.NumericUpDown();
            this.DTM_Release_Date = new System.Windows.Forms.DateTimePicker();
            this.Button_Apply = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_TDP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Techprocess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Threads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Cores)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Количество ядер:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Количество потоков:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(104, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Сокет:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(102, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Серия:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата выпуска:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Техпроцесс в нм:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Категория:";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(62, 305);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Частота в ГГц:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Компания:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Тепловыделение:";
            // 
            // TextBox_Socket
            // 
            this.TextBox_Socket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Socket.Location = new System.Drawing.Point(154, 130);
            this.TextBox_Socket.Name = "TextBox_Socket";
            this.TextBox_Socket.Size = new System.Drawing.Size(204, 23);
            this.TextBox_Socket.TabIndex = 7;
            // 
            // TextBox_Frequency
            // 
            this.TextBox_Frequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Frequency.Location = new System.Drawing.Point(154, 302);
            this.TextBox_Frequency.Name = "TextBox_Frequency";
            this.TextBox_Frequency.Size = new System.Drawing.Size(204, 23);
            this.TextBox_Frequency.TabIndex = 13;
            // 
            // TextBox_Series
            // 
            this.TextBox_Series.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Series.Location = new System.Drawing.Point(154, 245);
            this.TextBox_Series.Name = "TextBox_Series";
            this.TextBox_Series.Size = new System.Drawing.Size(204, 23);
            this.TextBox_Series.TabIndex = 11;
            // 
            // CheckBox_iGPU
            // 
            this.CheckBox_iGPU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_iGPU.AutoSize = true;
            this.CheckBox_iGPU.Location = new System.Drawing.Point(154, 331);
            this.CheckBox_iGPU.Name = "CheckBox_iGPU";
            this.CheckBox_iGPU.Size = new System.Drawing.Size(52, 19);
            this.CheckBox_iGPU.TabIndex = 14;
            this.CheckBox_iGPU.Text = "iGPU";
            this.CheckBox_iGPU.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextBox_ProcessorName);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.NumericUpDown_TDP);
            this.groupBox1.Controls.Add(this.ComboBox_Category);
            this.groupBox1.Controls.Add(this.ComboBox_Company);
            this.groupBox1.Controls.Add(this.Button_Add_Category);
            this.groupBox1.Controls.Add(this.Button_Add_Company);
            this.groupBox1.Controls.Add(this.NumericUpDown_Techprocess);
            this.groupBox1.Controls.Add(this.NumericUpDown_Threads);
            this.groupBox1.Controls.Add(this.NumericUpDown_Cores);
            this.groupBox1.Controls.Add(this.DTM_Release_Date);
            this.groupBox1.Controls.Add(this.CheckBox_iGPU);
            this.groupBox1.Controls.Add(this.TextBox_Series);
            this.groupBox1.Controls.Add(this.TextBox_Frequency);
            this.groupBox1.Controls.Add(this.TextBox_Socket);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 361);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // TextBox_ProcessorName
            // 
            this.TextBox_ProcessorName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_ProcessorName.Location = new System.Drawing.Point(154, 73);
            this.TextBox_ProcessorName.Name = "TextBox_ProcessorName";
            this.TextBox_ProcessorName.Size = new System.Drawing.Size(204, 23);
            this.TextBox_ProcessorName.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 14;
            this.label11.Text = "Название:";
            // 
            // NumericUpDown_TDP
            // 
            this.NumericUpDown_TDP.Location = new System.Drawing.Point(154, 160);
            this.NumericUpDown_TDP.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.NumericUpDown_TDP.Name = "NumericUpDown_TDP";
            this.NumericUpDown_TDP.Size = new System.Drawing.Size(120, 23);
            this.NumericUpDown_TDP.TabIndex = 8;
            // 
            // ComboBox_Category
            // 
            this.ComboBox_Category.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Category.FormattingEnabled = true;
            this.ComboBox_Category.Location = new System.Drawing.Point(154, 44);
            this.ComboBox_Category.Name = "ComboBox_Category";
            this.ComboBox_Category.Size = new System.Drawing.Size(204, 23);
            this.ComboBox_Category.TabIndex = 3;
            // 
            // ComboBox_Company
            // 
            this.ComboBox_Company.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Company.FormattingEnabled = true;
            this.ComboBox_Company.Location = new System.Drawing.Point(154, 13);
            this.ComboBox_Company.Name = "ComboBox_Company";
            this.ComboBox_Company.Size = new System.Drawing.Size(204, 23);
            this.ComboBox_Company.TabIndex = 1;
            this.ComboBox_Company.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Company_SelectedIndexChanged);
            // 
            // Button_Add_Category
            // 
            this.Button_Add_Category.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Add_Category.BackgroundImage")));
            this.Button_Add_Category.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button_Add_Category.Location = new System.Drawing.Point(58, 44);
            this.Button_Add_Category.Name = "Button_Add_Category";
            this.Button_Add_Category.Size = new System.Drawing.Size(20, 20);
            this.Button_Add_Category.TabIndex = 2;
            this.Button_Add_Category.UseVisualStyleBackColor = true;
            this.Button_Add_Category.Click += new System.EventHandler(this.Button_Add_Category_Click);
            // 
            // Button_Add_Company
            // 
            this.Button_Add_Company.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Add_Company.BackgroundImage")));
            this.Button_Add_Company.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button_Add_Company.Location = new System.Drawing.Point(58, 13);
            this.Button_Add_Company.Name = "Button_Add_Company";
            this.Button_Add_Company.Size = new System.Drawing.Size(20, 20);
            this.Button_Add_Company.TabIndex = 0;
            this.Button_Add_Company.UseVisualStyleBackColor = true;
            this.Button_Add_Company.Click += new System.EventHandler(this.Button_Add_Company_Click);
            // 
            // NumericUpDown_Techprocess
            // 
            this.NumericUpDown_Techprocess.Location = new System.Drawing.Point(154, 275);
            this.NumericUpDown_Techprocess.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.NumericUpDown_Techprocess.Name = "NumericUpDown_Techprocess";
            this.NumericUpDown_Techprocess.Size = new System.Drawing.Size(120, 23);
            this.NumericUpDown_Techprocess.TabIndex = 12;
            // 
            // NumericUpDown_Threads
            // 
            this.NumericUpDown_Threads.Location = new System.Drawing.Point(154, 217);
            this.NumericUpDown_Threads.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.NumericUpDown_Threads.Name = "NumericUpDown_Threads";
            this.NumericUpDown_Threads.Size = new System.Drawing.Size(120, 23);
            this.NumericUpDown_Threads.TabIndex = 10;
            // 
            // NumericUpDown_Cores
            // 
            this.NumericUpDown_Cores.Location = new System.Drawing.Point(154, 189);
            this.NumericUpDown_Cores.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.NumericUpDown_Cores.Name = "NumericUpDown_Cores";
            this.NumericUpDown_Cores.Size = new System.Drawing.Size(120, 23);
            this.NumericUpDown_Cores.TabIndex = 9;
            // 
            // DTM_Release_Date
            // 
            this.DTM_Release_Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DTM_Release_Date.Location = new System.Drawing.Point(154, 102);
            this.DTM_Release_Date.Name = "DTM_Release_Date";
            this.DTM_Release_Date.Size = new System.Drawing.Size(204, 23);
            this.DTM_Release_Date.TabIndex = 6;
            // 
            // Button_Apply
            // 
            this.Button_Apply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Apply.Location = new System.Drawing.Point(12, 386);
            this.Button_Apply.Name = "Button_Apply";
            this.Button_Apply.Size = new System.Drawing.Size(209, 23);
            this.Button_Apply.TabIndex = 15;
            this.Button_Apply.Text = "Применить";
            this.Button_Apply.UseVisualStyleBackColor = true;
            this.Button_Apply.Click += new System.EventHandler(this.Button_Apply_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(233, 386);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(146, 23);
            this.Button_Cancel.TabIndex = 16;
            this.Button_Cancel.Text = "Отмена";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            // 
            // FormProcessor
            // 
            this.AcceptButton = this.Button_Apply;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(391, 419);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Button_Apply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProcessor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Добавление процессора";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_TDP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Techprocess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Threads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Cores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextBox_Socket;
        private System.Windows.Forms.TextBox TextBox_Frequency;
        private System.Windows.Forms.TextBox TextBox_Series;
        private System.Windows.Forms.CheckBox CheckBox_iGPU;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Button_Apply;
        private System.Windows.Forms.Button Button_Cancel;
        private System.Windows.Forms.DateTimePicker DTM_Release_Date;
        private System.Windows.Forms.NumericUpDown NumericUpDown_Threads;
        private System.Windows.Forms.NumericUpDown NumericUpDown_Cores;
        private System.Windows.Forms.NumericUpDown NumericUpDown_Techprocess;
        private System.Windows.Forms.Button Button_Add_Company;
        private System.Windows.Forms.Button Button_Add_Category;
        private System.Windows.Forms.ComboBox ComboBox_Category;
        private System.Windows.Forms.ComboBox ComboBox_Company;
        private System.Windows.Forms.NumericUpDown NumericUpDown_TDP;
        private System.Windows.Forms.TextBox TextBox_ProcessorName;
        private System.Windows.Forms.Label label11;
    }
}