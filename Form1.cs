﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class Form1 : Form
    {
        private Processor CurrentProcessor;
        // Конструкторы
        public Form1()
        {
            InitializeComponent();
            Companies.Items = new List<Company>();
            Categories.Items = new List<CategoryCPU>();
            Processors.Items = new List<Processor>();
            Companies.ItemAdded += AddTreeNode1;
            Categories.ItemAdded += AddTreeNode2;
            Processors.ItemAdded += AddTreeNode3;
            MenuItemSave.Click += MenuFileSave_Click;
            MenuItemLoad.Click += MenuFileLoad_Click;
            toolTip1.SetToolTip(label12, "Интегрированное графическое ядро");
        }
        // Методы
        #region Функции построения и удаления узлов TreeView
        /// <summary>
        /// Проверяет TreeNodeCollection на наличие узла с name.
        /// Если узел не был найден возвращает false, создаёт TreeNode(name) и записывает в <c>out treeNode</c>,
        /// иначе возвращается true и в treeNode записывает найденный узел.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="name"></param>
        /// <param name="treeNode"></param>
        /// <returns></returns>
        private bool CheckNode(in TreeNodeCollection collection, in string name, out TreeNode treeNode)
        {
            bool ret = false;
            treeNode = null;
            if (collection.Count > 0)
            {
                // Если нашли node с именем name, то записываем его в treeNode
                foreach (TreeNode node in collection)
                {
                    if (node.Text == name)
                    {
                        ret = true;
                        treeNode = node;
                        break;
                    }
                }
            }
            // иначе создаём новый TreeNode(name)
            if (ret == false)
            {
                treeNode = new TreeNode(name);
            }

            return ret;
        }
        /// <summary>
        /// Функция <c>AddTreeNode1</c>.
        /// <para>Добавляет в <c>TreeView</c> компанию (1 уровень дерева).</para>
        /// </summary>
        /// <param name="obj">Объект <c>Company</c></param>
        private void AddTreeNode1(object obj)
        {
            Company company = (Company)obj;
            if (!CheckNode(TreeView_Data.Nodes, company.CompanyName, out TreeNode node1))
            {
                node1.Tag = company;
                TreeView_Data.Nodes.Add(node1);
            }
            else
            {
                _ = MessageBox.Show("Компания " + company.CompanyName + " уже существует");
            }

        }
        /// <summary>
        /// Функция <c>AddTreeNode2</c>.
        /// <para>Добавляет в <c>TreeView</c> категорию (2 уровень дерева). Если компании (1 уровень дерева) нет,
        /// то она будет добавлена</para>
        /// </summary>
        /// <param name="obj">Объект <c>CategoryCPU</c></param>
        private void AddTreeNode2(object obj)
        {
            CategoryCPU category = (CategoryCPU)obj;
            // Нашли компанию
            if (CheckNode(TreeView_Data.Nodes, category.CompanyName, out TreeNode node1))
            {
                // Нашли категорию
                if (CheckNode(node1.Nodes, category.Category, out TreeNode node2))
                {
                    _ = MessageBox.Show("Категория " + category.Category + " уже существует в компании " + category.CompanyName);
                }
                // Не нашли категорию
                else
                {
                    node2.Tag = category;
                    node1.Nodes.Add(node2);
                    node1.Expand();
                }
            }
            // Не нашли компанию, добавить
            else
            {
                AddTreeNode1(Companies.GetCompany(category.CompanyName));
                AddTreeNode2(category);
            }
        }
        /// <summary>
        /// Функция <c>AddTreeNode3</c>.
        /// <para>Добавляет в <c>TreeView</c> процессор (3 уровень дерева). Если компании (1 уровень дерева)
        /// и категории (2 уровень дерева) нет, то они будут добавлены</para>
        /// </summary>
        /// <param name="obj">Объект <c>Processor</c></param>
        private void AddTreeNode3(object obj)
        {
            Processor processor = (Processor)obj;
            // Нашли компанию
            if (CheckNode(TreeView_Data.Nodes, processor.CompanyName, out TreeNode node1))
            {
                // Нашли категорию
                if (CheckNode(node1.Nodes, processor.Category, out TreeNode node2))
                {
                    // Нашли процессор
                    if (CheckNode(node2.Nodes, processor.ProcessorName, out TreeNode node3))
                    {
                        _ = MessageBox.Show("Процессор " + processor.ProcessorName + " уже существует в компании " + processor.CompanyName
                            + "и категории " + processor.Category);
                    }
                    // Не нашли процессор
                    else
                    {
                        node3.Tag = processor;
                        node2.Nodes.Add(node3);
                        node2.Expand();
                    }
                }
                // Не нашли категорию
                else
                {
                    AddTreeNode2(Categories.GetCategoryCPU(processor.Category));
                    AddTreeNode3(processor);
                }
            }
            // Не нашли компанию, добавить
            else
            {
                AddTreeNode1(Companies.GetCompany(processor.CompanyName));
                AddTreeNode2(Categories.GetCategoryCPU(processor.Category));
                AddTreeNode3(processor);
            }
        }
        /// <summary>Функция DeleteTreeNode3
        /// <para>Удаляет экземпляр класса <c>Procesor</c> из списка <c>Processors.Items</c> и удаляет соответствующий
        /// узел из дерева TreeView</para>
        /// </summary>
        /// <param name="node3">Узел дерева (3 уровень)</param>
        /// <param name="parrentCollection">Коллекция <c>ThreeNodeCollection</c>, к которой относится
        /// <c>node3</c></param>
        private void DeleteTreeNode3(TreeNode node3, TreeNodeCollection parrentCollection)
        {
            Processors.RemoveProcessor((Processor)node3.Tag);
            node3.Remove();
            parrentCollection.Remove(node3);
        }
        /// <summary>Функция DeleteTreeNode2
        /// <para>Удаляет экземпляр класса <c>CategoryCPU</c> из списка <c>Categories.Items</c>
        /// и удаляет соответствующий узел из дерева TreeView</para>
        /// </summary>
        /// <param name="node2">Узел дерева (2 уровень)</param>
        /// <param name="parrentCollection">Коллекция <c>ThreeNodeCollection</c>, к которой относится <c>node2</c></param>
        private void DeleteTreeNode2(TreeNode node2, TreeNodeCollection parrentCollection)
        {
            if (node2.GetNodeCount(false) > 0)
            {
                for (int i = 0; i < node2.Nodes.Count;)
                {
                    DeleteTreeNode3(node2.Nodes[i], node2.Nodes);
                }
            }
            Categories.RemoveCategoryCPU((CategoryCPU)node2.Tag);
            node2.Remove();
            parrentCollection.Remove(node2);
        }
        /// <summary>Функция DeleteTreeNode1
        /// <para>Удаляет экземпляр класса <c>Company</c> из списка <c>Companies.Items</c>
        /// и удаляет соответствующий узел из дерева TreeView</para>
        /// </summary>
        /// <param name="node1">Узел дерева (1 уровень)</param>
        /// <param name="parrentCollection">Коллекция <c>ThreeNodeCollection</c>, к которой относится <c>node1</c></param>
        private void DeleteTreeNode1(TreeNode node1, TreeNodeCollection parrentCollection)
        {
            if (node1.GetNodeCount(false) > 0)
            {
                for (int i = 0; i < node1.Nodes.Count;)
                {
                    DeleteTreeNode2(node1.Nodes[i], node1.Nodes);
                }
            }
            Companies.RemoveCompany((Company)node1.Tag);
            node1.Remove();
            parrentCollection.Remove(node1);
        }
        /// <summary>Функция BuildTreeView
        /// <para>Строит TreeView по данным, которые храняться в свойстве <c>Items</c>
        /// классов <c>Companies</c>, <c>Categories</c> и <c>Processors</c></para>
        /// </summary>
        private void BuildTreeView()
        {
            TreeView_Data.Nodes.Clear();

            foreach (Company company in Companies.Items)
            {
                AddTreeNode1(company);
            }

            foreach (CategoryCPU category in Categories.Items)
            {
                AddTreeNode2(category);
            }

            foreach (Processor processor in Processors.Items)
            {
                AddTreeNode3(processor);
            }
        }
        #endregion
        /// <summary>
        /// Выводит информацию о процессоре в соответствующие поля
        /// </summary>
        /// <param name="processor"></param>
        private void PrintProcessorInfo(Processor processor)
        {
            Label_Company.Text = processor.CompanyName;
            Label_Category.Text = processor.Category;
            Label_ProcessorName.Text = processor.ProcessorName;
            Label_Cores.Text = processor.Cores.ToString();
            Label_Threads.Text = processor.Threads.ToString();
            Label_Frequency.Text = processor.ClockFrequency.ToString();
            Label_ReleaseDate.Text = processor.ReleaseDate.ToString();
            Label_Series.Text = processor.Series;
            Label_Socket.Text = processor.Socket;
            Label_TDP.Text = processor.TDP.ToString();
            Label_Techprocess.Text = processor.Techprocess.ToString();
            Label_IGPU.Text = processor.IGPU ? "Есть": "Нет";
        }
        /// <summary>Функция ClearProcessorInfo
        /// <para>Очищает поля, где выводится информация о процессоре</para>
        /// </summary>
        private void ClearProcessorInfo()
        {
            Label_Company.Text = "Не задано";
            Label_Category.Text = "Не задано";
            Label_ProcessorName.Text = "Не задано";
            Label_Cores.Text = "Не задано";
            Label_Threads.Text = "Не задано";
            Label_Frequency.Text = "Не задано";
            Label_ReleaseDate.Text = "Не задано";
            Label_Series.Text = "Не задано";
            Label_Socket.Text = "Не задано";
            Label_TDP.Text = "Не задано";
            Label_Techprocess.Text = "Не задано";
            Label_IGPU.Text = "Не задано";
        }
        #region Функции редактирования компании, категории и процессора
        /// <summary>
        /// Редактирует поле companyName у "категорий" (2 уровень дерева) и "процессоров" (3 уровень дерева),
        /// котрые относятся к компании. Выбирает объекты, проходя вглубь по ветке "компании" (1 уровень дерева).
        /// </summary>
        /// <param name="collection">Узлы 2 уровня дерева (категории)</param>
        /// <param name="newName">Новое название кампании</param>
        private void EditCompanyNameInNodesTag(TreeNodeCollection collection, string newCompany)
        {
            foreach (TreeNode node in collection)
            {
                // Если есть процессоры. (3 уровень дерева)
                if (node.Nodes.Count > 0)
                {
                    EditCompanyNameInNodesTag(node.Nodes, newCompany);
                }
                ((Company)node.Tag).CompanyName = newCompany;
            }
        }
        /// <summary>
        /// Редактирует поле <c>Category</c> у "процессоров" (3 уровень дерева),
        /// котрые относятся к категории. Выбирает объекты, проходя по ветке "категория" (2 уровень дерева).
        /// </summary>
        /// <param name="collection">Узлы 3 уровня дерева (процессоры)</param>
        /// <param name="newCategory">Новое название категории</param>
        private void EditCategoryInNodesTag(TreeNodeCollection collection, string newCategory)
        {
            foreach (TreeNode node in collection)
            {
                ((CategoryCPU)node.Tag).Category = newCategory;
            }
        }
        /// <summary>
        /// Редактирует объект <c>Company</c>
        /// </summary>
        /// <param name="node">Узел, который содержит объект</param>
        private void EditCompany(TreeNode node)
        {
            FormCompany form = new FormCompany();
            Company company = (Company)node.Tag;

            form.Owner = this;
            form.Text = "Редактирование компании";
            form.TextBox_Company.Text = company.CompanyName;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                company.CompanyName = form.NewCompany.CompanyName;
                node.Text = company.CompanyName;
                EditCompanyNameInNodesTag(node.Nodes, company.CompanyName);
            }
        }
        /// <summary>
        /// Редактирует объект <c>CategoryCPU</c>
        /// </summary>
        /// <param name="node">Узел, который содержит объект</param>
        private void EditCategory(TreeNode node)
        {
            CategoryCPU category = (CategoryCPU)node.Tag;
            FormCategory form = new FormCategory(category);

            form.Owner = this;
            form.Text = "Редактирование категории";
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                category.Category = form.NewCategory.Category;
                node.Text = category.Category;
                EditCategoryInNodesTag(node.Nodes, category.Category);
            }
        }
        /// <summary>
        /// Редактирует объект <c>Processor</c>
        /// </summary>
        /// <param name="node">Узел, который содержит объект</param>
        private void EditProcessor(TreeNode node)
        {
            Processor processor = (Processor)node.Tag;
            FormProcessor form = new FormProcessor(processor);

            form.Owner = this;
            form.Text = "Редактирование процессора";
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                Processors.CopyProcessor(form.NewProcessor, processor);
                CurrentProcessor = processor;

                node.Text = CurrentProcessor.ProcessorName;
                PrintProcessorInfo(CurrentProcessor);
            }
        }
        #endregion
        // Обработчики
        private void Button_AddCompany_Click(object sender, EventArgs e)
        {
            FormCompany form = new FormCompany();
            form.Owner = this;

            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                Company company = form.NewCompany;
                Companies.AddCompany(company);
            }
        }
        private void Button_AddCategory_Click(object sender, EventArgs e)
        {
            if (Companies.Items.Count == 0)
            {
                _ = MessageBox.Show("Добавьте компанию");
                return;
            }

            FormCategory form = new FormCategory();

            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                CategoryCPU category = form.NewCategory;
                Categories.AddCategoryCPU(category);
            }
        }
        private void Button_AddProcessor_Click(object sender, EventArgs e)
        {
            FormProcessor form = new FormProcessor();
            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                CurrentProcessor = form.NewProcessor;
                Processors.AddProcessor(CurrentProcessor);
                PrintProcessorInfo(CurrentProcessor);
            }
        }
        private void Button_Edit_Click(object sender, EventArgs e)
        {
            TreeNode node = TreeView_Data.SelectedNode;

            if (node == null)
            {
                _ = MessageBox.Show("Не выбрано ни одного элемента");
                return;
            }

            switch(node.Level)
            {
                // Редактировать компанию
                case 0:
                    EditCompany(node);
                    break;
                // Редактировать категорию
                case 1:
                    EditCategory(node);
                    break;
                // Редактировать процессор
                case 2:
                    EditProcessor(node);
                    break;
            }
        }
        private void Button_Delete_Click(object sender, EventArgs e)
        {
            TreeNode node = TreeView_Data.SelectedNode;
            TreeNodeCollection collection;

            if (node == null)
            {
                _ = MessageBox.Show("Не выбрано ни одного элемента");
                return;
            }

            switch (node.Level)
            {
                case 0:
                    if (node.GetNodeCount(true) > 0)
                    {
                        DialogResult dialog = MessageBox.Show(
                            "При удалении \"компании\" будут удалены все \"категории\" и \"процессоры\". Продолжить?",
                            "Сообщение",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);

                        if (DialogResult.Yes == dialog)
                        {
                            DeleteTreeNode1(node, TreeView_Data.Nodes);
                        }
                    }
                    else
                    {
                        DeleteTreeNode1(node, TreeView_Data.Nodes);
                    }
                    break;
                case 1:
                    collection = node.Parent.Nodes;
                    if (node.GetNodeCount(true) > 0)
                    {
                        DialogResult dialog = MessageBox.Show(
                            "При удалении \"категории\" будут удалены все \"процессоры\". Продолжить?",
                            "Сообщение",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);

                        if (DialogResult.Yes == dialog)
                        {
                            DeleteTreeNode2(node, collection);
                        }
                    }
                    else
                    {
                        DeleteTreeNode2(node, collection);
                    }
                    break;
                case 2:
                    collection = node.Parent.Nodes;
                    DeleteTreeNode3(node, collection);
                    break;
            }
        }
        private void TreeView_Data_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 2)
            {
                PrintProcessorInfo((Processor)e.Node.Tag);
            }
            else
            {
                ClearProcessorInfo();
            }
        }

        private void Button_SortCompany_Click(object sender, EventArgs e)
        {
            Companies.Items.Sort();
            BuildTreeView();
        }

        private void Button_SortCategory_Click(object sender, EventArgs e)
        {
            Categories.Items.Sort();
            BuildTreeView();
        }

        private void Button_SortProcessor_Click(object sender, EventArgs e)
        {
            Processors.Items.Sort();
            BuildTreeView();
        }

        private void MenuFileSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Файлы xml (*.xml)|*.xml";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                MySerializer.SaveToXml(saveFileDialog.FileName);

        }
        private void MenuFileLoad_Click(object sender, EventArgs e)
        {
            TreeView_Data.Nodes.Clear();
            Companies.Items.Clear();
            Categories.Items.Clear();
            Processors.Items.Clear();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Файлы xml (*.xml)|*.xml";
            openFileDialog.RestoreDirectory = true;


            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    MySerializer.LoadFromXml(openFileDialog.FileName);
                }
                catch (Exception error)
                {
                    _ = MessageBox.Show(error.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
