﻿
namespace CourseWork
{
    partial class LongestWord
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_FileName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Word = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_CountSymbol = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_FileName
            // 
            this.label_FileName.AutoSize = true;
            this.label_FileName.Location = new System.Drawing.Point(0, 0);
            this.label_FileName.Name = "label_FileName";
            this.label_FileName.Size = new System.Drawing.Size(107, 15);
            this.label_FileName.TabIndex = 0;
            this.label_FileName.Text = "Перетащиет файл";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Слово:";
            // 
            // textBox_Word
            // 
            this.textBox_Word.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Word.Location = new System.Drawing.Point(51, 30);
            this.textBox_Word.Multiline = true;
            this.textBox_Word.Name = "textBox_Word";
            this.textBox_Word.Size = new System.Drawing.Size(206, 42);
            this.textBox_Word.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Количество букв:";
            // 
            // label_CountSymbol
            // 
            this.label_CountSymbol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_CountSymbol.AutoSize = true;
            this.label_CountSymbol.Location = new System.Drawing.Point(109, 75);
            this.label_CountSymbol.Name = "label_CountSymbol";
            this.label_CountSymbol.Size = new System.Drawing.Size(13, 15);
            this.label_CountSymbol.TabIndex = 4;
            this.label_CountSymbol.Text = "0";
            // 
            // UserControl2
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_CountSymbol);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_Word);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_FileName);
            this.Name = "UserControl2";
            this.Size = new System.Drawing.Size(261, 102);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.UserControl2_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.UserControl2_DragEnter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_FileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Word;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_CountSymbol;
    }
}
