﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormCompany : Form
    {
        public Company NewCompany;
        public FormCompany()
        {
            InitializeComponent();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TextBox_Company.Text))
            {
                _ = MessageBox.Show("Введите название");
            }
            else
            {
                if (Companies.FindItem(TextBox_Company.Text))
                {
                    _ = MessageBox.Show("Компания с таким именем уже существует");
                }
                else
                {
                    NewCompany = new Company(TextBox_Company.Text);
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
