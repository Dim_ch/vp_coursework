﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class LongestWord : UserControl
    {
        public LongestWord()
        {
            InitializeComponent();
        }

        private void UserControl2_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) &&
                ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move))
                e.Effect = DragDropEffects.Move;
        }

        private void UserControl2_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                // В files хранятся пути к папкам и файлам
                ResetControls();
                if (files.Length > 1)
                {
                    _ = MessageBox.Show("Перетащите 1 файл");
                    return;
                }
                label_FileName.Text = files[0];

                if (!AnalizeFile(files[0]))
                    ResetControls();
            }
        }
        private void ResetControls()
        {
            textBox_Word.Text = null;
            label_CountSymbol.Text = "0";
            label_FileName.Text = "Перетащиет файл";
        }
        private bool AnalizeFile(string fileName)
        {
            bool ret = false;
            if (Path.GetExtension(fileName) != ".txt")
            {
                _ = MessageBox.Show("Выберите файл с расширением \".txt\"");
                ResetControls();
                return ret;
            }

            ret = true;
            try
            {
                using (StreamReader sr = new StreamReader(fileName, Encoding.Default))
                {
                    char[] separators = new char[] { ' ', '.', '(', ')', '[', ']' };

                    string line, word = "Файл пустой";
                    int count = 1;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] subs = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                        if (count == 1)
                        {
                            --count;
                            word = FindMaxWord(subs);
                        }
                        else
                        {
                            line = FindMaxWord(subs);
                            word = line.Length > word.Length ? line : word;
                        }
                    }
                    textBox_Word.Text += word;
                    label_CountSymbol.Text = (word == "Файл пустой") ? "0" : word.Length.ToString();
                }
            }
            catch(Exception error)
            {
                _ = MessageBox.Show(error.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ret = false;
            }
            return ret;
        }
        private string FindMaxWord(string[] words)
        {
            string word = "";
            if (words.Length > 0)
            {
                word = words[0];
                for (int i = 0; i < words.Length - 1; ++i)
                {
                    if (words[i].Length <= words[i + 1].Length)
                        word = words[i + 1];
                }
            }
            return word;
        }
    }
}
